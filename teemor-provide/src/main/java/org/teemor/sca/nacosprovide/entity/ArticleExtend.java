package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity(name = "tb_article_extend")
public class ArticleExtend {
    /**
     * 文章ID
     */
    @Id
    private Integer atcId;

    /**
     * 浏览量
     */
    private Integer browseNum;

    /**
     * 赞数量
     */
    private Integer praiseNum;

    /**
     * 评论数量
     */
    private Integer commentNum;
}