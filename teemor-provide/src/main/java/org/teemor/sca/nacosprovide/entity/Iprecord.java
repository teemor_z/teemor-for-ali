package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Iprecord {
    @Id
    @GeneratedValue
    private Integer id;

    private String ip;

    private Date date;

    private String addr;
}