package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Footprint {
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 文章ID
     */
    private Integer atcId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 用户所在地
     */
    private String userAddress;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 数据状态
     */
    private Integer dataState;
}