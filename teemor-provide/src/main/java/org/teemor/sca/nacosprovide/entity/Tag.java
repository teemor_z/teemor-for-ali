package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Tag {
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 标签名
     */
    private String name;

    /**
     * 标签类型:   0-文章来源，1-普通标签，2-专题标签
     */
    private Integer tagType;

    /**
     * 样式
     */
    private String style;

    /**
     * 数据状态：0-启用，1-停用
     */
    private Integer dataState;
}