package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Subscribe {
    /**
     * 主键
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 订阅内容ID
     */
    private Integer subId;

    /**
     * 订阅类型：1-文章，2-分组，3-专题
     */
    private Integer subType;

    /**
     * 提醒邮件地址
     */
    private String notifyEmail;

    /**
     * 订阅码，取消订阅时需要校验
     */
    private String keyWord;

    /**
     * 用户Id
     */
    private Integer userId;

    /**
     * 订阅时IP
     */
    private String firstIp;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 取消订阅时间
     */
    private Date cancelTime;

    /**
     * 数据状态：0-启用，1-删除
     */
    private Integer dataState;
}