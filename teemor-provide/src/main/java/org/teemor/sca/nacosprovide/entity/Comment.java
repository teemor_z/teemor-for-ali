package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Comment {
    /**
     * 评论ID
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 文章ID
     */
    private Integer atcId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户邮箱
     */
    private String userEmail;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 数据状态
     */
    private Integer dataState;

    /**
     * 评论内容
     */
    private String commentContent;
}