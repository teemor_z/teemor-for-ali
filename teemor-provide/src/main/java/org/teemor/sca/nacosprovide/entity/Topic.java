package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Topic {
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 分类名
     */
    private String topicName;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 描述
     */
    private String topicDesc;

    /**
     * 分类类型：1-分类，2-书籍
     */
    private Integer topicType;

    /**
     * 分类级别
     */
    private Integer topicLevel;

    /**
     * 父级ID
     */
    private Integer parentId;

    /**
     * 父级分组名
     */
    private String parentName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 删除时间
     */
    private Date dropTime;

    /**
     * 数据状态
     */
    private Integer dataState;

    /**
     * 分组下的文章总数
     */
    private Integer articleTotal;
}