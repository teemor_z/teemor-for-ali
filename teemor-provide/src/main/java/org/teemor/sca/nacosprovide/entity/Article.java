package org.teemor.sca.nacosprovide.entity;

import lombok.Data;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Article {
    /**
     * 文章ID
     */
    @Id
    @GeneratedValue
    private Integer atcId;

    /**
     * 作者ID
     */
    @Column(name = "user_id", nullable = false, length = 11)
    private Integer userId;

    /**
     * 作者名
     */
    @Column(name = "user_name", nullable = false)
    private String userName;

    /**
     * 分类主题ID
     */
    @Column(name = "topic_id", nullable = false, length = 11)
    private Integer topicId;

    /**
     * 分类主题名
     */
    @Column(name = "topic_name", nullable = false)
    private String topicName;

    /**
     * 文章标题
     */
    @Column(name = "title", nullable = false)
    private String title;

    /**
     * 博客封面图路径
     */
    @Column(name = "cover", nullable = false)
    private String cover;

    /**
     * 博客状态：0-草稿，1-已发布
     */
    @Column(name = "atc_state", nullable = false)
    private Integer atcState;

    /**
     * 创建时间
     */
    @Column(name = "create_time", nullable = false)
    private Date createTime;

    /**
     * 删除时间
     */
    @Column(name = "drop_time", nullable = false)
    private Date dropTime;

    /**
     * 数据状态
     */
    @Column(name = "data_state", nullable = false)
    private Integer dataState;

    /**
     * 文章内容
     */
    @Column(name = "content", nullable = false)
    private String content;

    /**
     * 文章摘要
     */
    @Column(name = "summary", nullable = false)
    private String summary;

    /**
     * 定时时间
     */
    @Column(name = "sched_time", nullable = false)
    private Date schedTime;

    /**
     * 启用定时
     */
    @Column(name = "sched", nullable = false)
    private Integer sched;


//    private Integer copy;

//    private String copyId;

    /**
     * 目录
     */
    @Column(name = "dir", nullable = false)
    private String dir;
}