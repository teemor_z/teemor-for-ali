package org.teemor.sca.nacosprovide.server;

import org.apache.dubbo.config.annotation.Service;
import org.teemor.sca.apis.SampleApi;

/**
 * @Description:
 * @author zhoulk
 * @date: 2020/3/12
 */
@Service
public class SampleServiceImpl implements SampleApi {

    @Override
    public String echoServer(String param) {
        return param;
    }
}
