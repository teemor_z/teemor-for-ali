package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Modifyhis {
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 文章ID
     */
    private Integer atcId;

    /**
     * 修改内容
     */
    private String modifyData;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 数据状态
     */
    private Integer dataState;
}