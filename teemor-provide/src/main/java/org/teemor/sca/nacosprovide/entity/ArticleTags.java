package org.teemor.sca.nacosprovide.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class ArticleTags {
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * 文章ID
     */
    @Column(nullable = false)
    private Integer atcId;

    /**
     * 标签ID
     */
    @Column(nullable = false)
    private Integer tagId;

    /**
     * 数据状态
     */
    private Integer dataState;
}