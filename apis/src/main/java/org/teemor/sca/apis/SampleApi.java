package org.teemor.sca.apis;

/**
 * @Description:
 * @author zhoulk
 * @date: 2020/3/12
 */
public interface SampleApi {

    String echoServer(String param);
}
